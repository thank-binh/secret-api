import { Entity, Column, OneToMany } from "typeorm";
import { AbstractBaseEntity } from "./abstract-base-entity";
import { MemberEntity } from "./members.entity";

@Entity('roles')
export class RoleEntity extends AbstractBaseEntity {
  @Column('varchar', {
    nullable: false,
    length: 100,    
  })
  name: string;

  @OneToMany(
    type => MemberEntity,
    member => member.roleId,
  )
  members: MemberEntity[];
}