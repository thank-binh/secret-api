import { Entity, ManyToOne, JoinColumn, Column } from "typeorm";
import { AbstractBaseEntity } from "./abstract-base-entity";
import { UserEntity } from "./users.entity";
import { ProjectEntity } from "./projects.entity";
import { RoleEntity } from "./roles.entity";

@Entity('members')
export class MemberEntity extends AbstractBaseEntity {
  @ManyToOne(
    type => UserEntity,
    user => user.members,
    { eager: true },
  )
  @JoinColumn({
    name: 'user_id'
  })
  userId: UserEntity;

  @ManyToOne(
    type => ProjectEntity,
    project => project.members,
    { eager: true },
  )
  @JoinColumn({
    name: 'project_id'
  })
  projectId: ProjectEntity;

  @ManyToOne(
    type => RoleEntity,
    role => role.members,
    { eager: true },
  )
  @JoinColumn({
    name: 'role_id'
  })
  roleId: RoleEntity;

  @Column('boolean', {
    nullable: false,    
    default: true,
    })
  status: boolean;
}