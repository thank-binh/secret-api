import { Entity, Column, OneToMany } from "typeorm";
import { AbstractBaseEntity } from "./abstract-base-entity";
import { TaskEntity } from "./tasks.entity";
import { MemberEntity } from "./members.entity";

@Entity('projects')
export class ProjectEntity extends AbstractBaseEntity {
  @Column('varchar', {
    nullable: false,
    length: 100,    
  })
  customer: string;

  @Column('varchar', {
    nullable: false,
    length: 255,    
  })
  name: string;

  @Column('varchar', {
    nullable: false,
    length: 100,
    name: 'mail_group',
  })
  mailGroup: string;

  @Column('date', {
    nullable: false,
    name: 'start_date',    
  })
  startDate: string;

  @Column('date', {
    nullable: false,
    name: 'end_date',
  })
  endDate: string;

  @Column('tinyint', {
    nullable: false,    
    default: 0,
    name: 'manager',    
  })
  manager: number;

  @Column('tinyint', {
    nullable: false,    
    default: true,
    name: 'status',    
    })
  status: boolean;

  @OneToMany(
    type => TaskEntity,
    task => task.projectId,
  )
  tasks: TaskEntity[];

  @OneToMany(
    type => MemberEntity,
    member => member.roleId,
  )
  members: MemberEntity[];
}