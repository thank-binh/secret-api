import { Entity, Column, OneToMany } from "typeorm";
import { AbstractBaseEntity } from './abstract-base-entity';
import { IsEmail } from 'class-validator';
import { TaskEntity } from "./tasks.entity";
import { MemberEntity } from "./members.entity";

@Entity('users')
export class UserEntity extends AbstractBaseEntity {
  @IsEmail()
  @Column('varchar', {
    nullable: false,
    length: 100,    
    unique: true,
    })  
  mail: string;

  @Column('varchar', {
    nullable: false,
    length: 100,
    name: 'full_name',
    })
  fullName: string;

  @Column('varchar', {
    nullable: false,
    length: 32,    
    })
  password: string;

  @Column('date', {
    nullable: false,
    name: 'date_of_birth',
    })
  dateOfBirth: Date;

  @Column('boolean', {
    nullable: false,    
    default: true,       
    })
  status: boolean;

  @Column('varchar', {
    nullable: true,
    length: 1000,    
    })
  avatar: string;

  @OneToMany(
    type => TaskEntity,
    task => task.userId,
  )
  tasks: TaskEntity[];

  @OneToMany(
    type => MemberEntity,
    member => member.roleId,
  )
  members: MemberEntity[];  
}