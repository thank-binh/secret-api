import { Entity, Column, ManyToOne, JoinColumn } from "typeorm";
import { AbstractBaseEntity } from "./abstract-base-entity";
import { UserEntity } from "./users.entity";
import { ProjectEntity } from "./projects.entity";

@Entity('tasks')
export class TaskEntity extends AbstractBaseEntity {
  @Column('varchar', {
    nullable: false,
    length: 255,    
  })
  title: string;

  @Column('varchar', {
    nullable: false,
    length: 1000,    
  })
  content: string;

  @Column('smallint', {
    nullable: false, 
    default: 0,    
  })
  progress: number;

  @Column('boolean', {
    nullable: false,    
    default: true,        
  })
  status: boolean;

  @ManyToOne(
    type => UserEntity,
    user => user.tasks,
    { eager: true },    
  )
  @JoinColumn({        
    name: 'user_id'
  })
  userId: UserEntity;

  @ManyToOne(
    type => ProjectEntity,
    project => project.tasks,
    { eager: true },    
  )
  @JoinColumn({        
    name: 'project_id'
  })
  projectId: ProjectEntity;
}