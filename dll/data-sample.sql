-- Users table
INSERT INTO `users` (mail,full_name,password,date_of_birth) VALUES ('lorem@a.a','ADMIN','123456789','2020-02-20');
INSERT INTO `users` (mail,full_name,password,date_of_birth) VALUES ('lorem@b.a','ADMIN 2','123456789','2020-02-20');
INSERT INTO `users` (mail,full_name,password,date_of_birth) VALUES ('lorem@c.a','ADMIN 3','123456789','2020-02-20');
INSERT INTO `users` (mail,full_name,password,date_of_birth) VALUES ('lorem@d.a','ADMIN 4','123456789','2020-02-20');
INSERT INTO `users` (mail,full_name,password,date_of_birth) VALUES ('lorem@e.a','ADMIN 5','123456789','2020-02-20');

-- Roles talbe
insert into roles (name) values ('SM');
insert into roles (name) values ('PM');
insert into roles (name) values ('Dev');
insert into roles (name) values ('Test');
insert into roles (name) values ('QA');

-- Projects table
insert into projects (customer, name, mail_group, start_date, end_date, manager) values ('Jeep', 'Patriot', 'poblein0@salon.com', '2020-05-01', '2019-11-08', 1);
insert into projects (customer, name, mail_group, start_date, end_date, manager) values ('Mitsubishi', 'Diamante', 'jgonnelly1@squidoo.com', '2020-06-17', '2020-05-26', 2);
insert into projects (customer, name, mail_group, start_date, end_date, manager) values ('Chevrolet', 'Corvette', 'loconnell2@amazonaws.com', '2019-11-04', '2019-11-18', 3);
insert into projects (customer, name, mail_group, start_date, end_date) values ('Volkswagen', 'Phaeton', 'lantoszewski3@paypal.com', '2019-12-06', '2020-02-29');
insert into projects (customer, name, mail_group, start_date, end_date) values ('Oldsmobile', 'Silhouette', 'ahavick4@loc.gov', '2020-08-05', '2020-05-21');

-- Tasks table
insert into tasks (title, content, progress, status, user_id, project_id) values ('IyR2TowHqXdHu1', 'lMPRzne5WeeZxlM08Dpe5zuVrEpo', 77, true, 1, 1);
insert into tasks (title, content, progress, status, user_id, project_id) values ('zmBdFlyFOR3wxc', 'gPPvrvr6RYMuQVNMMIcuJrSoQ77x', 62, true, 2, 2);
insert into tasks (title, content, progress, status, user_id, project_id) values ('UcJfjf0jO24n6Q', 'wj71f1KTOVusIm8ulaU6DSFNwkNx', 99, true, 3, 3);
insert into tasks (title, content, progress, status, user_id, project_id) values ('kuDL9X1CbMcJ3V', 'KjDGaVhqonoQxIqkzjkpAqEbKg9j', 3, false, 1, 4);
insert into tasks (title, content, progress, status, user_id, project_id) values ('daBpEYpyPHpcZP', 'u5g9z3N5GoL2CznD33xzaRWxdOkI', 58, true, 2, 5);
insert into tasks (title, content, progress, status, user_id, project_id) values ('P1DRb8aFmSbQTG', '75GASyL9RKksW7aA3ivaRQJBWcYf', 22, true, 3, 1);
insert into tasks (title, content, progress, status, user_id, project_id) values ('jsLvVFU0rOJ7Ib', '45EwH9coF0HVMONHxhL4WUNklWYL', 33, true, 4, 2);
insert into tasks (title, content, progress, status, user_id, project_id) values ('KwXgDDrnOTN0zR', 'eww1gvGR7ndOA7VftUKZJCDHH1Ux', 66, false, 1, 3);
insert into tasks (title, content, progress, status, user_id, project_id) values ('hGsdYsQCs619Kn', 'e7oKJmFa46gYTLVhWQt3qn1fLvso', 7, true, 3, 4);
insert into tasks (title, content, progress, status, user_id, project_id) values ('WUSXic85bPD0A8', 'ZHuUrBwdi4KVAtaXew5keIuSu0M2', 6, true, 3, 5);

-- Members table
INSERT INTO `db_mail_support`.`members` (`user_id`, `project_id`, `role_id`, `status`) VALUES ('1', '1', '1', '1');
INSERT INTO `db_mail_support`.`members` (`user_id`, `project_id`, `role_id`, `status`) VALUES ('2', '2', '2', '1');
INSERT INTO `db_mail_support`.`members` (`user_id`, `project_id`, `role_id`, `status`) VALUES ('3', '3', '1', '1');
INSERT INTO `db_mail_support`.`members` (`user_id`, `project_id`, `role_id`, `status`) VALUES ('4', '4', '3', '0');
INSERT INTO `db_mail_support`.`members` (`user_id`, `project_id`, `role_id`, `status`) VALUES ('1', '5', '2', '0');